People who made large contributions to the EnMAP-Box

Earth Observation Lab, Humboldt-Universität zu Berlin, Germany

* Andreas Rabe <andreas.rabe@geo.hu-berlin.de>
* Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
* Franz Schug <franz.schug@geo.hu-berlin.de>
* Matthias Held <matthias.held@hu-berlin.de>
* Sebastian van der Linden <sebastian.linden@geo.hu-berlin.de>
* Akpona Okujeni <akpona.okujeni@geo.hu-berlin.de>
* Sam Cooper <sam.cooper@geo.hu-berlin.de>
* Stefan Ernst <stefan.ernst@geo.hu-berlin.de>
* Fabian Thiel <fabian.thiel@geo.hu-berlin.de>

Ludwig-Maximilians-Universität München, Germany

* Martin Danner <martin.danner@iggf.geo.uni-muenchen.de>
* Matthias Wocher <m.wocher@iggf.geo.uni-muenchen.de>

GFZ German Research Centre For Geosciences, Potsdam, Germany

* Daniel Scheffler <daniel.scheffler@gfz-potsdam.de>
* Christian Mielke <chmielke@gfz-potsdam.de>
* Theres Kuester <theres.kuester@gfz-potsdam.de>
* Saskia Förster <foerster@gfz-potsdam.de>
