.. _PyCharm: https://www.jetbrains.com/pycharm
.. _PyDev: https://www.pydev.org
.. _OSGeo4W: https://www.osgeo.org/projects/osgeo4w
.. _pip: https://pip.pypa.io/en/stable
