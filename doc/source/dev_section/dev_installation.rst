Installation
############

.. toctree::
   :maxdepth: 1

   dev_installation_slim.rst
   dev_installation_detailed.rst