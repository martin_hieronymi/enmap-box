Application Tutorials
#####################

.. toctree::
   :maxdepth: 1

   urban_unmixing/tutorial.rst
   biomass_regression/practical.rst
   ocean_colour/onns.rst
   engeomap/tutorial_engeomap.rst
   Manual Retrieval of Vegetation Variables using IVVRM <https://enmap-box-lmu-vegetation-apps.readthedocs.io/en/latest/tutorials/IVVRM_tut.html>


.. * `Manual Retrieval of Vegetation Variables using IVVRM <https://enmap-box-lmu-vegetation-apps.readthedocs.io/en/latest/tutorials/IVVRM_tut.html>`_ (external link)
