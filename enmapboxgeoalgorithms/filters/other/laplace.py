from scipy.ndimage.filters import laplace

function = lambda array: laplace(array)
