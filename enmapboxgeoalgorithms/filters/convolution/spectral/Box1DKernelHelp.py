from enmapboxgeoalgorithms.provider import Help, Link

helpAlg = Help(text='Applies Box1DKernel.')

helpCode = Help(text='Python code. See {} for information on different parameters.',
                links=[Link('http://docs.astropy.org/en/stable/api/astropy.convolution.Box1DKernel.html',
                            'astropy.convolution.Box1DKernel')])

