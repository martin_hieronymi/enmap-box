from astropy.convolution import TrapezoidDisk2DKernel

kernel = TrapezoidDisk2DKernel(radius=3, slope=1)
